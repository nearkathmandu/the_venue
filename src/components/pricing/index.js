import React, { Component } from 'react';

import MyBytton from '../utils/MyButton';
import Zoom from 'react-reveal/Zoom';

class Pricing extends Component {
    state={
        prices: [100,150,250],
        positions:['Balcony','Medium','Star'],
        desc:[
            'Hwellgfeh athis is 100 dollat awseome thing that yoiyu kncie maheiurehuefhddgd vhcbvdddjd',
            'hello this i s150 when there is medhgfdh fdthing that make you beautiful and some random stufff ',
            'Yello hatkjhslfd fdup you\'re a star a buring start and you are fjdhjhfd thjshfd dfhhghjvkj iojdhgofoyes yes'
        ],
        linkto: ['http://google.com','http://youtube.com','http://twitter.com'],
        delay:[500,0,500]

    }

    showBoxes = () => (
        this.state.prices.map((box,i)=>(
            <Zoom delay={this.state.delay[i]} key={i}>
                <div className="pricing_item">
                    <div className="pricing_inner_wrapper">
                        <div className="pricing_title">
                            <span>${this.state.prices[i]}</span>
                            <span>{this.state.positions[i]}</span>
                        </div>
                        <div className="pricing_description">
                            {this.state.desc[i]}

                        </div>
                        <div className="pricing_buttons">
                            <MyBytton 
                                text="Purchase"
                                bck="#ffa800"
                                color="#ffffff"
                                link={this.state.linkto[i]}
                            />
                        </div>

                    </div>

                </div>
                
            </Zoom>
           
        ))
    )

    render() {
        return (
            <div className="bck_black">
                <div className="center_wrapper pricing_section">
                    <h2>Pricing</h2>
                    <div className="pricing_wrapper">
                        {this.showBoxes()}
                    </div>  
                </div>
                
            </div>
        );
    }
}

export default Pricing;