import React from 'react';

const Location = () => {
    return (
        <div className="location_wrapper">

        
        <iframe 
            title="Map"
            src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14063.595911400951!2d83.9569831!3d28.2103817!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc27a3dac2c253ff2!2sTrisara!5e0!3m2!1sen!2snp!4v1546658611975" 
            width="100%" 
            height="500" 
            frameBorder="0" 
            
            allowFullScreen></iframe>

            <div className="location_tag">
                <div>Location</div>

            </div>

        
            
        </div>
    );
};

export default Location; 