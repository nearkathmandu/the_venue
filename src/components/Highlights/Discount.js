import React, { Component } from 'react';

import Fade from 'react-reveal/Fade';
import Slide from 'react-reveal/Slide'

import MyButton from '../utils/MyButton';

class Discount extends Component {

    state = {
        discountStart:0,
        discountEnd:30
    }

    porcentage = () => {
        if(this.state.discountStart < this.state.discountEnd){
            this.setState({
                discountStart: this.state.discountStart + 1
            })

        }
    }

    componentDidUpdate() {
        setTimeout(()=>{
            this.porcentage()
        },30)
    }



    render() {
        return (
            <div className="center_wrapper">
                <div className="discount_wrapper">
                    <Fade
                        onReveal={()=> this.porcentage()}
                    >
                        <div className="discount_porcentage">
                            <span>{this.state.discountStart}%</span>
                            <span>OFF</span>
                        </div>
                    </Fade>

                    <Slide right>

                        <div className="discount_description">
                            <h3>Purchage ticekets before 20th JUNE</h3>
                            <p> therefore does not include many of the tools some developers might consider necessary to build an application. This allows the choice of whichever libraries the developer prefers to accomplish tasks such as performing network access or local data storage. Common patterns of usage have emerged as the library matures</p>
                
                           <MyButton 
                               text="Purchage tickets"
                               bck="#ffa800"
                               color="#ffffff"
                               link="http://google.com"
                           />
                            
                        </div>
                    </Slide> 
                </div>
            </div>
        );
    }
}

export default Discount;